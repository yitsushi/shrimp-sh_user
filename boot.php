<?php
PluginStore::register('sh_user', 'ShUser', 'lib');
PluginStore::register('sh_user', 'User', 'controller');

Router::get('/user/:id.:format', 'view', 'user');