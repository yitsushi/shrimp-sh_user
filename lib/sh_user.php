<?php
class ShUser extends Controller {
  protected $this_is_plugin = true;
  protected $plugin_name    = 'sh_user';

  public function view($id) {
    $this->page->assign('uid', $id);
    $this->render('view');
  }
}